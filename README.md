# Amazon Lockers.

## Introduction

Les consignes Amazon sont des points de collecte autonomes pour les commandes Amazon. On en trouve un peu partout en France. Certains sont ouverts 24h/24 7j/7 tandis que d'autres sont accessibles à des heures d'ouverture déterminées. C'est le cas par exemple pour les consignes Amazon que l'on trouve dans le centre commercial So-Ouest de Levallois-Perret. La consigne est accessible uniquement pendant les jours et heures d'ouverture du centre commercial.

https://www.amazon.fr/b?ie=UTF8&node=5193561031

Chaque consigne Amazon est nommée et se compose de 35 à 135 casiers de tailles différentes (Small, Medium et Large). Chaque étagère contient 10 casiers (1 x Large, 2 x Medium et 7 x Small). Il y a aussi une étagère qui n'offre que 5 casiers (1 x Large, 1 x Medium et 3 x Small) et un écran tactile. L'écran tactile permet au client de saisir le code à 6 chiffres et lettres pour ouvrir le casier associé et récupérer son colis. Au moment de leur commande, les clients du site Amazon peuvent choisir de se faire livrer leur colis dans un point de collecte autonome. Lorsque le livreur Amazon a déposé le colis dans un casier, un e-mail est automatiquement envoyé au client pour lui signaler qu'il peut venir récupérer son paquet. L'e-mail contient un code à 6 chiffres et lettres que le client doit saisir sur le moniteur de la consigne pour ouvrir le casier associé. Si le client ne vient pas récupérer son colis dans les 3 jours ouvrés après la livraison, alors le colis est retourné à Amazon et le client est remboursé. Un client peut aussi utiliser les consignes Amazon pour retourner un colis à Amazon. Par exemple, si le produit reçu n'est pas le bon ou s'il est endommagé.

## Travail demandé

Pour cet exercice, il est demandé de modéliser le fonctionnement d'une consigne Amazon de 35 casiers (3 étagères de 10 casiers + 1 étagère de 5 casiers avec le moniteur central). La mission consiste à développer une petite interface en ligne de commande avec le composant Console de Symfony qui permet de modéliser la livraison et l'enlèvement de colis. Nous vous demandons de développer trois commandes console qui réalisent respectivement chacune des tâches suivantes :

1. Connaître l'état de la consigne.
2. Livrer un colis.
3. Réceptionner un colis.

Ces trois fonctionnalités sont décrites dans le détail dans la section suivante.

## Fonctionnalités

### Connaître l'état de la consigne

En mode normal, la commande doit afficher seulement des statistiques simples :

* Le nombre de casiers de taille Small
* Le nombre de casiers de taille Medium
* Le nombre de casiers de taille Large
* Le nombre de casiers Libres
* Le nombre de casiers Occupés

En mode verbeux (option `--verbose`), la commande doit lister en plus l'ensemble des casiers de la consigne et indiquer pour chacun son état actuel : son numéro, statut d'occupation (libre ou occupé) et la référence du colis. S'il est occupé, le code de déverrouillage à 6 chiffres et lettres doit être affiché ainsi que la référence unique du colis.

### Livrer un colis

En tant que livreur Amazon, je dois pouvoir livrer un colis dans un casier.

La commande prend en arguments obligatoires la référence unique du colis, la taille du colis (S, M ou L) et l'adresse e-mail du client. Le système doit être capable de trouver un casier libre susceptible d'acceuillir le colis. Si, pour un colis de taille S, il n'existe plus de casiers de type S disponibles, alors le système doit choisir un casier plus grand disponible. Si aucun casier n'est disponible pour accueillir le colis, le système doit générer une erreur. Pour un casier libre, le système doit générer un code de déverrouillage unique composé de 6 chiffres et lettres, et envoyer ce code par e-mail au client (adresse e-mail passée en argument de la commande).

Contraintes :

* Le code de déverrouillage est toujours unique.

### Réceptionner un colis

En tant que client, je dois être capable de retirer mon colis à l'aide d'un code de déverrouillage.

La commande prend en argument obligatoire le code de deverrouillage. Le système vérifie qu'un casier est associé à ce code. Le code ouvre le casier et le rend de nouveau libre pour un futur nouveau colis. La commande affiche le numéro de référence du colis ou une erreur en cas de code invalide.
