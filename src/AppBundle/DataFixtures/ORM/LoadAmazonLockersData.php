<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\AmazonDeposit;
use AppBundle\Entity\AmazonPacket;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Ramsey\Uuid\Uuid;

class LoadAmazonLockersData extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $manager->persist(new AmazonDeposit('Amazon Main Locker'));

        $manager->persist(new AmazonPacket(Uuid::fromString('c48e28fe-8588-4d6d-98db-b25e952c5b28'), 'Amazon Main Locker', 'randomuser123@gmail.com', 'M'));
        $manager->persist(new AmazonPacket(Uuid::fromString('30bbf160-dda3-44d5-8e0f-8fc564a3a4ef'), 'Amazon Main Locker', 'fan.randomuser123@gmail.fr', 'S'));
        $manager->persist(new AmazonPacket(Uuid::fromString('a5db6161-d2f0-4b6a-803a-956d364d47a1'), 'Amazon Main Locker', 'randomuser122.jan@gmail.com', 'S'));
        $manager->persist(new AmazonPacket(Uuid::fromString('1c28dabd-ca38-414d-b84a-68619704bd4d'), 'Amazon Main Locker', 'randomuser1443@orange.com', 'L'));

        $manager->flush();
    }
}
