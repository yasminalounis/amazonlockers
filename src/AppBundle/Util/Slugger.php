<?php
namespace AppBundle\Util;

class Slugger
{
    public static function slugify(string $value): string

    {
        return preg_replace('/\s+/', '-', mb_strtolower(trim(strip_tags($value)), 'UTF-8'));
    }
}