<?php

namespace AppBundle\Util;

final class LockerSize
{
    private const SIZES = ['S', 'M', 'L'];

    private $size;

    public function __construct(string $size)
    {
        if (!in_array($size, self::SIZES, true)) {
            throw new \InvalidArgumentException('Size d\'ont match');
        }

        $this->size = $size;
    }

    public static function fromString(string $size): self
    {
        if (in_array($size, self::SIZES)) {
            return new self($size);
        }
    }

    public static function makeSmallest(): self
    {
        return new self(self::SIZES[0]);
    }

    public function nextBigger(): ?self
    {
        $k = array_search($this->size, self::SIZES, true) + 1;

        return isset(self::SIZES[$k]) ? new self(self::SIZES[$k]) : null;
    }

    public function getSize(): string
    {
        return $this->size;
    }

    public function equals(self $other): bool
    {
        return $this->size === $other->getSize();
    }
}