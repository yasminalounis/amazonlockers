<?php

namespace AppBundle\Util;

use AppBundle\Event\PacketDeliveredEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class CustomerMailer implements EventSubscriberInterface
{
    private $twig;
    private $mailer;

    public function __construct(
        \Twig_Environment $twig,
        \Swift_Mailer $mailer
    ) {
        $this->twig = $twig;
        $this->mailer = $mailer;
    }

    public function onPacketDelivered(PacketDeliveredEvent $event): void
    {
        $packet = $event->getPacketUuid();
        $deposit = $event->getDeposit();
        $unlock = $event->getUnlockCode()->getCode();
        $body = $this->twig->render('mail/parcel_delivered.txt.twig', [
            'packet' => $packet,
            'deposit' => $deposit,
            'unlockCode' => $unlock,
        ]);


        $message = $this->mailer->createMessage();
        $message->setSubject('Your packet has been delivered!');
        $message->setBody($body);
        $message->setTo($event->getCustomerEmail());
        $message->setFrom('noreply@amazon.com');

        $this->mailer->send($message);
    }

    public static function getSubscribedEvents()
    {
        return [
            PacketDeliveredEvent::PACKAGE_DELIVERED => 'onPacketDelivered',
        ];
    }
}