<?php

namespace AppBundle\Util;

final class UnlockCode
{
    private $code;

    public function __construct(string $code)
    {
        if (!preg_match('/^[A-Z0-9]{6}$/', $code)) {
            throw new \InvalidArgumentException('the unlock code must be a set 6 lettres and numbers');
        }

        $this->code = $code;
    }

    public static function generate(): self
    {
        $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';

        $code = '';
        for ($i = 1; $i <= 6; $i++) {
            $k = random_int(0, strlen($chars) - 1);
            $code.= $chars[$k];
        }

        return new self($code);
    }

    public function equals(self $other): bool
    {
        return $this->code === $other->getCode();
    }


    public function getCode(): string
    {
        return $this->code;
    }
}