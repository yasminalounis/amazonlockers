<?php

namespace AppBundle\Command;

use AppBundle\Entity\AmazonDeposit;
use AppBundle\Entity\Locker;
use AppBundle\Repository\AmazonDepositRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class DepositStatusCommand extends Command
{
    private $depositRepository;

    /**
     * @var SymfonyStyle
     */
    private $io;

    public function __construct(AmazonDepositRepository $depositRepository)
    {
        parent::__construct(null);

        $this->depositRepository = $depositRepository;
    }
    protected function configure()
    {
        $this->setName('amazonlocker:deposit:status')
            ->setDescription('The Amazon main deposit statistics');
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->io = new SymfonyStyle($input, $output);
        $this->io->title('AMAZON Deposit of lockers Status');
        $deposit = $this->depositRepository->byName(AmazonDeposit::DEPOSIT_NAME);
        $this->io->write(sprintf('The Status of the deposit %s :', $deposit->getName()));
        $this->io->success(sprintf('The number of Small lockers is %d.',  $deposit->getNumberOfSmallLockers()));
        $this->io->success(sprintf('The number of Medium lockers is %d.', $deposit->getNumberOfMediumLockers()));
        $this->io->success(sprintf('The number of Large lockers  is %d.', $deposit->getNumberOfLargeLockers()));
        $this->io->success(sprintf('The number of Available lockers  is %d.', $deposit->getNumberOfAvailableLockers()));
        $this->io->success(sprintf('The number of Occupied lockers  is %d.', $deposit->getNumberOfOccupiedLockers()));
        //the list of all lockers
        if ($this->io->isVerbose()){
            foreach ($deposit->getLockers() as $locker) {
                /** @var Locker $locker */
                if ($locker->isAvailable()){
                    $this->io->writeln(sprintf('The lockers %s has the size %s and it is %s.', $locker->getNumber(), $locker->getSize()->getSize(),  $locker->getStatus()));
                } else {
                    $this->io->writeln(sprintf(
                        'The lockers %s has the size %s and it is %s. The unlock code is %s and the packet stored in the locker has the number %s.',
                        $locker->getNumber(),
                        $locker->getSize()->getSize(),
                        $locker->getStatus(),
                        $locker->getUnlockCode()->getCode(),
                        $locker->getPacketUuid()->toString())
                    );
                }
            }
        }
    }
}