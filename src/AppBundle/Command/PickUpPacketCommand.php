<?php

namespace AppBundle\Command;

use AppBundle\Services\AmazonPacketPickUpService;
use AppBundle\Util\UnlockCode;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class PickUpPacketCommand extends Command
{
    private $pickupService;

    /**
     * @var SymfonyStyle
     */
    private $io;

    public function __construct(AmazonPacketPickUpService $pickupService)
    {
        parent::__construct(null);

        $this->pickupService = $pickupService;
    }
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->io = new SymfonyStyle($input, $output);
        $this->io->title('AMAZON Pick Up service');
    }
    protected function configure()
    {
        $this->setName('amazonlocker:packet:pickup')
            ->setDescription('pick up a packet at a deposit')
            ->addArgument('unlockcode', InputArgument::OPTIONAL, 'The Unlock code');
    }

    protected function interact(InputInterface $input, OutputInterface $output)
    {
        if (!$packet = $input->getArgument('unlockcode')) {
            $input->setArgument('unlockcode', $this->io->ask('Provide the Unlock code'));
        }

    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $unlockCode = new UnlockCode($input->getArgument('unlockcode'));
        $packetUuid = $this->pickupService->pickUpPacket($unlockCode);

        $this->io->success(sprintf('Packet has been picked up at the deposit %s. The packet number id %s', AmazonPacketPickUpService::DEPOSIT_NAME, $packetUuid->toString()));
    }
}