<?php

namespace AppBundle\Command;

use AppBundle\Entity\AmazonDeposit;
use AppBundle\Services\AmazonPacketDeliveryService;
use AppBundle\Util\LockerSize;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class DeliverPacketCommand extends Command
{
    private $deliveryService;

    /**
     * @var SymfonyStyle
     */
    private $io;

    public function __construct(AmazonPacketDeliveryService $deliveryService)
    {
        parent::__construct(null);

        $this->deliveryService = $deliveryService;
    }
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->io = new SymfonyStyle($input, $output);
        $this->io->title('AMAZON Delivery');
    }
    protected function configure()
    {
        $this->setName('amazonlocker:packet:deliver')
            ->setDescription('Deliver a packet at a deposit')
            ->addArgument('packet', InputArgument::OPTIONAL, 'The packet reference')
            ->addArgument('size', InputArgument::OPTIONAL, 'The size of the AmazonPacket S, M, L')
            ->addArgument('email', InputArgument::OPTIONAL, 'The customer e-mail');
    }

    protected function interact(InputInterface $input, OutputInterface $output)
    {
        if (!$packet = $input->getArgument('packet')) {
            $input->setArgument('packet', $this->io->ask('Provide the packet UUID'));
        }

        if (!$size = $input->getArgument('size')) {
            $input->setArgument('size', $this->io->ask('Provide the packet size'));
        }

        if (!$email = $input->getArgument('email')) {
            $input->setArgument('email', $this->io->ask('Provide the customer email'));
        }
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $packetUuid = Uuid::fromString($input->getArgument('packet'));
        $size = LockerSize::fromString($input->getArgument('size'));
        $email = $input->getArgument('email');
        $this->deliveryService->deliverPacket($packetUuid, $size, $email);

        $this->io->success(sprintf('Packet %s has been delivered at the deposit %s.', $packetUuid, AmazonDeposit::DEPOSIT_NAME));
    }
}