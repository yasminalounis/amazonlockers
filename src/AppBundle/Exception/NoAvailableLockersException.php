<?php

namespace AppBundle\Exception;

use AppBundle\Util\LockerSize;
use AppBundle\Entity\AmazonDeposit;

class NoAvailableLockersException extends \DomainException
{
    public function __construct(string $message, \Throwable $previous = null)
    {
        parent::__construct($message, 0, $previous);
    }

    public static function noAvailableForSize(AmazonDeposit $deposit, LockerSize $size, \Throwable $previous = null): self
    {
        return new static(
            sprintf('No available lockers found for the packet size %s at the deposit %s.', $size->getSize(), $deposit->getName()),
            $previous
        );
    }
}