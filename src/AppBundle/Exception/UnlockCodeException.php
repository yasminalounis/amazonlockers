<?php

namespace AppBundle\Exception;

use AppBundle\Entity\AmazonDeposit;
use AppBundle\Util\UnlockCode;
use DomainException;

class UnlockCodeException extends DomainException
{
    public function __construct(string $message, \Throwable $previous = null)
    {
        parent::__construct($message, 0, $previous);
    }

    public static function createNoLockerFound(UnlockCode $unlockCode, AmazonDeposit $deposit, \Throwable $previous = null): self
    {
        return new static(
            sprintf('No occupied lockers found for unlock code %s at deposit %s.', $unlockCode->getCode(), $deposit->getName()),
            $previous
        );
    }

    public static function createAlreadyUsedException(UnlockCode $unlockCode, AmazonDeposit $deposit, \Throwable $previous = null): self
    {
        return new static(
            sprintf('Unlock code %s is already in use at deposit %s.', $unlockCode->getCode(), $deposit->getName()),
            $previous
        );
    }
}