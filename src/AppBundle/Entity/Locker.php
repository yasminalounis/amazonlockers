<?php
/**
 * Created by PhpStorm.
 * User: lounis
 * Date: 13/03/2018
 * Time: 18:22
 */

namespace AppBundle\Entity;

use AppBundle\Util\LockerSize;
use AppBundle\Util\UnlockCode;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity()
 */
class Locker
{
    private const AVAILABLE = 'available';
    private const OCCUPIED = 'occupied';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\AmazonDeposit", inversedBy="lockers")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $deposit;

    /**
     * @ORM\Column(length=3)
     */
    private $number;

    /**
     * @ORM\Column(length=1)
     */
    private $size;

    /**
     * @ORM\Column(length=20)
     */
    private $status;

    /**
     * @ORM\Column(length=6, nullable=true)
     */
    private $unlockCode;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $expiresAt;

    /**
     * @ORM\Column(type="uuid", nullable=true)
     */
    private $packetUuid;

    public function __construct(
        AmazonDeposit $deposit,
        string $number,
        string $size
    ) {
        $this->deposit = $deposit;
        $this->number = $number;
        $this->size = $size;
        $this->status = self::AVAILABLE;
    }

    public function isLockedByCode(UnlockCode $unlockCode): bool
    {
        if (!$this->unlockCode) {
            return false;
        }

        return $unlockCode->equals(new UnlockCode($this->unlockCode));
    }

    public function getUnlockCode(): UnlockCode
    {
        return new UnlockCode($this->unlockCode);
    }


    public function getPacketUuid(): UuidInterface
    {
        return $packetUuid = clone $this->packetUuid;;
    }


    public function unlock(): void
    {
        if (self::OCCUPIED !== $this->status) {
            throw new \DomainException('Locker must be not available');
        }

        $this->status = self::AVAILABLE;
        $this->unlockCode = null;
        $this->expiresAt = null;
    }

    public function pickupPacket(): UuidInterface
    {
        $packetUuid = clone $this->packetUuid;

        $this->packetUuid = null;

        return $packetUuid;
    }

    public function getSize(): LockerSize
    {
        return LockerSize::fromString($this->size);
    }

    public function getNumber(): string
    {
        return $this->number;
    }

    public function getAmazonDeposit(): string
    {
        return $this->deposit->getName();
    }

    public function isAvailable(): bool
    {
        return self::AVAILABLE === $this->status;
    }

    public function ifSize(LockerSize $size): bool
    {
        return $size->equals($this->getSize());
    }

    public function getRange(): string
    {
        return $this->number[0];
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function deliverPacket(AmazonPacket $packet, UnlockCode $unlockCode): void
    {
        if (self::AVAILABLE !== $this->status) {
            throw new \DomainException('Locker not available');
        }

        $this->packetUuid = $packet->getUuid();
        $this->unlockCode = $unlockCode->getCode();
        $date = new \DateTime();
        $this->expiresAt = $date->add(new \DateInterval('P3D'));
        $this->status = self::OCCUPIED;
    }
}
