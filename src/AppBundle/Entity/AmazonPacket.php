<?php
/**
 * Created by PhpStorm.
 * User: lounis
 * Date: 13/03/2018
 * Time: 18:12
 */

namespace AppBundle\Entity;

use AppBundle\Util\LockerSize;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity()
 */
class AmazonPacket
{
    /**
     * @ORM\Id
     * @ORM\Column(type="uuid")
     */
    private $uuid;

    /**
     * @ORM\Column(length=1)
     */
    private $size;

    /**
     * @ORM\Column(length=100)
     */
    private $customerEmail;

    /**
     * @ORM\Column(length=50)
     */
    private $deliveryDeposit;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deliveredAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $pickedAt;


    public function __construct(
        UuidInterface $uuid,
        string $deliveryDeposit,
        string $customerEmail,
        string $size
    ) {
        $this->uuid = $uuid;
        $this->size = $size;
        $this->customerEmail = $customerEmail;
        $this->deliveryDeposit = $deliveryDeposit;
    }

    public function getCustomerEmail(): string
    {
        return $this->customerEmail;
    }

    public function getDeliveryDeposit(): string
    {
        return $this->deliveryDeposit;
    }

    public function getBestLockerSize(): LockerSize
    {
        return LockerSize::fromString($this->size);
    }

    public function getUuid(): UuidInterface
    {
        return $this->uuid;
    }

    public function delivered(): void
    {
        if ($this->deliveredAt) {
            throw new \DomainException('AmazonPacket has already been delivered.');
        }

        $this->deliveredAt = new \DateTime();
    }
    public function picked(): void
    {
        if ($this->pickedAt) {
            throw new \DomainException('AmazonPacket has already been picked.');
        }

        $this->pickedAt = new \DateTime();
    }
}
