<?php
/**
 * Created by PhpStorm.
 * User: lounis
 * Date: 13/03/2018
 * Time: 18:47
 */

namespace AppBundle\Entity;

use AppBundle\Exception\UnlockCodeException;
use AppBundle\Exception\NoAvailableLockersException;
use AppBundle\Util\LockerSize;
use AppBundle\Util\Slugger;
use AppBundle\Util\UnlockCode;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AmazonDepositRepository")
 * @ORM\Table(
 *   uniqueConstraints={
 *     @ORM\UniqueConstraint(name="deposit_slug_unique", columns="slug")
 *   }
 * )
 */
class AmazonDeposit
{
    const DEPOSIT_NAME = 'Amazon Main Locker';
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(length=50)
     */
    private $name;

    /**
     * @ORM\Column(length=50)
     */
    private $slug;

    /**
     * @ORM\Column(type="smallint")
     */
    private $numberOfAvailableLockers;

    /**
     * @ORM\Column(type="smallint")
     */
    private $numberOfOccupiedLockers;
    /**
     * @ORM\Column(type="smallint")
     */
    private $numberOfSmallLockers;

    /**
     * @ORM\Column(type="smallint")
     */
    private $numberOfMediumLockers;
    /**
     * @ORM\Column(type="smallint")
     */
    private $numberOfLargeLockers;


    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(
     *   targetEntity="AppBundle\Entity\Locker",
     *   mappedBy="deposit",
     *   cascade={"all"}
     * )
     * @ORM\OrderBy({"number": "ASC"})
     */
    private $lockers;


    public function __construct(
        string $name
    ) {
        $this->setName($name);
        $this->setLockers();
    }


    private function setName(string $name): void
    {
        $this->name = ucfirst($name);
        $this->slug = Slugger::slugify($name);
    }
    /**
     * this function is used to create all the 3 rages of 10 lockers and a range of 5 lockers
     * they are initialized to be not occupied
     */
    private function setLockers(): void
    {
        $firstRange = 'SSSML';
        $rangeOf10Lockers = 'SSSSSSSMML';

        $ranges = 'BCD';

        $lockers = new ArrayCollection();
        //build the first range locker
        $this->buildLocker($lockers, 'A', $firstRange);
        //build the 3 range of 10 lockers
        foreach (str_split($ranges) as $k => $letter) {
            $this->buildLocker($lockers, $letter, $rangeOf10Lockers);
        }

        $this->lockers = $lockers;
        $this->numberOfAvailableLockers = 35;
        $this->numberOfOccupiedLockers = 0;
    }

    private function buildLocker(ArrayCollection $lockers, string $letter, string $layout): void
    {
        foreach (str_split($layout) as $k => $size) {
            $position = $k + 1;
            $number = $letter . $position;
            $lockers->add(new Locker($this, $number, $size));
            switch ($size){
                case 'S':
                    $this->numberOfSmallLockers++;
                    break;
                case 'M':
                    $this->numberOfMediumLockers++;
                    break;
                case 'L':
                    $this->numberOfLargeLockers++;
                    break;
            }
        }
    }

    /**
     *
     */
    public function getLockers()
    {
        return $this->lockers;
    }

    /**
     * @throws UnlockCodeException
     */
    private function findLockerByCode(UnlockCode $unlockCode): Locker
    {
        $lockers = $this->lockers->filter(function (Locker $locker) use ($unlockCode) {
            return $locker->isLockedByCode($unlockCode);
        });

        if (1 !== count($lockers)) {
            throw UnlockCodeException::createNoLockerFound($unlockCode, $this);
        }

        return $lockers->first();
    }

    public function deliverPacket(AmazonPacket $packet, LockerSize $size, string $email): ?UnlockCode
    {
        $unlockCode = $this->generateUnlockCode();

        $this->ensureDeliveryDeposit($packet);
        $this->ensureUniqueUnlockCode($unlockCode);
        $locker = $this->findAvailableLocker($packet);
        $locker->deliverPacket($packet, $unlockCode);
        $packet->delivered();

        $this->numberOfAvailableLockers--;
        $this->numberOfOccupiedLockers++;

        return $unlockCode;
    }
    public function claimPacket(UnlockCode $unlockCode): UuidInterface
    {
        $locker = $this->findLockerByCode($unlockCode);
        $locker->unlock();

        $packetUuid = $locker->pickupPacket();

        $this->numberOfAvailableLockers++;
        $this->numberOfOccupiedLockers--;

        return $packetUuid;
    }


    private function ensureUniqueUnlockCode(UnlockCode $unlockCode): void
    {
        if ($this->isUnlockCodeUsed($unlockCode)) {
            throw UnlockCodeException::createAlreadyUsedException($unlockCode, $this);
        }
    }

    private function ensureDeliveryDeposit(AmazonPacket $packet): void
    {
        $slug = Slugger::slugify($packet->getDeliveryDeposit());

        if ($slug !== $this->slug) {
            throw new \DomainException('Unexpected delivery deposit!');
        }
    }

    private function generateUnlockCode(): UnlockCode
    {
        do {
            $code = UnlockCode::generate();
        } while ($this->isUnlockCodeUsed($code));

        return $code;
    }

    private function isUnlockCodeUsed(UnlockCode $unlockCode): bool
    {
        try {
            $this->findLockerByCode($unlockCode);
        } catch (UnlockCodeException $e) {
            return false;
        }

        return true;
    }

    private function findAvailableLocker(AmazonPacket $packet): Locker
    {
        $size = $packet->getBestLockerSize();

        while (null !== $size) {
            if ($locker = $this->findAvailableForSize($size)) {
                return $locker;
            }

            $size = $size->nextBigger();
        }

        throw NoAvailableLockersException::noAvailableForSize($this, $packet->getBestLockerSize());
    }

    private function findAvailableForSize(LockerSize $size): ?Locker
    {
        $lockers = $this->lockers->filter(
            function (Locker $locker) use ($size) {
                return $locker->isAvailable() && $locker->ifSize($size);
            }
        );

        return count($lockers) > 1 ? $lockers->first() : null;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function getNumberOfAvailableLockers(): int
    {
        return $this->numberOfAvailableLockers;
    }

    public function getNumberOfOccupiedLockers(): int
    {
        return $this->numberOfOccupiedLockers;
    }

    public function getNumberOfSmallLockers(): int
    {
        return $this->numberOfSmallLockers;
    }
    public function getNumberOfMediumLockers(): int
    {
        return $this->numberOfMediumLockers;
    }
    public function getNumberOfLargeLockers(): int
    {
        return $this->numberOfLargeLockers;
    }


    public function getRanges(): array
    {
        $ranges = [];
        foreach ($this->lockers as $locker) {
            $ranges[$locker->getRange()][] = $locker;
        }

        return $ranges;
    }

    public function getRange(string $letter): array
    {
        return $this->getRanges()[$letter];
    }



}
