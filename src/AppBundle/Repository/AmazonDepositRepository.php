<?php

namespace AppBundle\Repository;

use AppBundle\Entity\AmazonDeposit;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;

class AmazonDepositRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, AmazonDeposit::class);
    }

    public function byName(string $name): ?AmazonDeposit{
        $query = $this
            ->createQueryBuilder('d')
            ->select('d, l')
            ->leftJoin('d.lockers', 'l')
            ->where('d.slug = :name')
            ->orWhere('LOWER(d.name) = :name')
            ->setParameter('name', mb_strtolower($name))
            ->getQuery()
        ;

        try {
            return $query->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
        }

        return null;
    }

    public function getLockers(AmazonDeposit $deposit): array {
        return $this
            ->createQueryBuilder('d')
            ->select('d, l')
            ->leftJoin('d.lockers', 'l')
            ->where('d.slug = :name')
            ->orWhere('LOWER(d.name) = :name')
            ->setParameter('name', mb_strtolower($deposit->getName()))
            ->getQuery()
            ->getArrayResult()
        ;
    }

}
