<?php

namespace AppBundle\Services;

use AppBundle\Entity\AmazonDeposit;
use AppBundle\Entity\AmazonPacket;
use AppBundle\Event\PacketDeliveredEvent;
use AppBundle\Util\LockerSize;
use Doctrine\Common\Persistence\ObjectManager;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class AmazonPacketDeliveryService
{

    private $dispatcher;
    private $amazonPacketRepository;
    private $entityManager;

    public function __construct(
        EventDispatcherInterface $dispatcher,
        ObjectManager $manager
    ) {
        $this->dispatcher = $dispatcher;
        $this->entityManager = $manager;
        $this->amazonPacketRepository = $manager->getRepository(AmazonPacket::class);
    }

    public function deliverPacket(
        UuidInterface $packetUuid,
        LockerSize $size,
        string $email
    ): void
    {
        $packet = $this->amazonPacketRepository->findOneBy(['uuid'=>$packetUuid]);
        if ($packet && $packet->getCustomerEmail() !== $email && $packet->getBestLockerSize() !== $size ) {
            throw new \RuntimeException('The packet found does not match with the size and the e-mail you entered');
        }

        $deposit = $this->entityManager->getRepository(AmazonDeposit::class)->byName(AmazonDeposit::DEPOSIT_NAME);
        if (!$packet) {
            $packet = new AmazonPacket($packetUuid, AmazonDeposit::DEPOSIT_NAME,  $email, $size);
            $this->entityManager->persist($packet);
        }
        $unlockCode = $deposit->deliverPacket($packet, $size, $email);

        $this->entityManager->flush();
        $this->dispatcher->dispatch(
            PacketDeliveredEvent::PACKAGE_DELIVERED,
            new PacketDeliveredEvent($packet, $unlockCode)
        );
    }
}