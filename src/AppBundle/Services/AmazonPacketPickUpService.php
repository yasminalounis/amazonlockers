<?php

namespace AppBundle\Services;

use AppBundle\Entity\AmazonDeposit;
use AppBundle\Entity\AmazonPacket;
use AppBundle\Event\PacketPickedEvent;
use AppBundle\Util\UnlockCode;
use Doctrine\Common\Persistence\ObjectManager;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class AmazonPacketPickUpService
{
    private $dispatcher;
    private $amazonPacketRepository;
    private $entityManager;

    public function __construct(
        EventDispatcherInterface $dispatcher,
        ObjectManager $manager
    ) {
        $this->dispatcher = $dispatcher;
        $this->entityManager = $manager;
        $this->amazonPacketRepository = $manager->getRepository(AmazonPacket::class);
    }

    public function pickUpPacket(
        UnlockCode $unlockCode
    ): ?UuidInterface
    {
        $deposit = $this->entityManager->getRepository(AmazonDeposit::class)->byName(AmazonDeposit::DEPOSIT_NAME);

        $packetUuid = $deposit->claimPacket($unlockCode);

        if (!$packet = $this->amazonPacketRepository->findOneBy(['uuid'=>$packetUuid])) {
            throw new \RuntimeException('No packet found!');
        } else {
            $packet->picked();
        }

        $this->entityManager->flush();
        $this->dispatcher->dispatch(
            PacketPickedEvent::PACKAGE_PICKED,
            new PacketPickedEvent($packet)
        );
        return $packetUuid;
    }
}