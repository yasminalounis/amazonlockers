<?php

namespace AppBundle\Event;

use AppBundle\Entity\AmazonPacket;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\EventDispatcher\Event;

class PacketPickedEvent extends Event
{
    const PACKAGE_PICKED = 'packet.picked';

    private $packet;
    private $deposit;

    public function __construct(AmazonPacket $packet)
    {
        $this->packet = $packet;
        $this->deposit = $packet->getDeliveryDeposit();
    }

    public function getPacketUuid(): UuidInterface
    {
        return $this->packet->getUuid();
    }

    public function getDeposit(): string
    {
        return $this->deposit;
    }
}