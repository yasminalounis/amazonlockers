<?php

namespace AppBundle\Event;

use AppBundle\Entity\AmazonPacket;
use AppBundle\Util\UnlockCode;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\EventDispatcher\Event;

class PacketDeliveredEvent extends Event
{
    const PACKAGE_DELIVERED = 'packet.delivered';

    private $packet;
    private $email;
    private $deposit;
    private $unlock;

    public function __construct(AmazonPacket $packet, UnlockCode $unlockCode)
    {
        $this->packet = $packet;
        $this->email = $packet->getCustomerEmail();
        $this->deposit = $packet->getDeliveryDeposit();
        $this->unlock = $unlockCode;
    }

    public function getPacketUuid(): UuidInterface
    {
        return $this->packet->getUuid();
    }

    public function getCustomerEmail(): string
    {
        return $this->email;
    }
    public function getDeposit(): string
    {
        return $this->deposit;
    }
    public function getUnlockCode(): UnlockCode
    {
        return $this->unlock;
    }
}